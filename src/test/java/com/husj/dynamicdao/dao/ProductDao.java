package com.husj.dynamicdao.dao;

import com.husj.dynamicdao.model.ProductVo;

/**
 * ProductDao
 *
 * @author shengjun.hu
 * @date 2021/6/16
 */
public interface ProductDao extends BaseDao<ProductVo> {
}
