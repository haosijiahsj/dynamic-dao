package com.husj.dynamicdao.dao;

import com.husj.dynamicdao.model.EntityPo;

/**
 * QueryDao1
 *
 * @author shengjun.hu
 * @date 2021/6/10
 */
public interface EntityDao extends BaseDao<EntityPo> {
}
