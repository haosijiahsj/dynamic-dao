package com.husj.dynamicdao.service;

import com.husj.dynamicdao.dao.ProductDao;
import com.husj.dynamicdao.model.ProductVo;
import com.husj.dynamicdao.slavedao.ProductSlaveDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * ProductService
 *
 * @author shengjun.hu
 * @date 2021/6/16
 */
@Service
public class ProductService {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ProductSlaveDao productSlaveDao;

    public void save(ProductVo productVo) {
        productDao.save(productVo);

        List<ProductVo> productVos = new ArrayList<>();
        productVos.add(productVo);
        productSlaveDao.batchSave(productVos);
    }

    public void batchSave(List<ProductVo> productVos) {
        productDao.batchSave(productVos);
    }

}
