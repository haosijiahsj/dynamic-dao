package com.husj.dynamicdao.model;

import com.husj.dynamicdao.annotations.mapping.Column;
import com.husj.dynamicdao.annotations.mapping.Id;
import com.husj.dynamicdao.annotations.mapping.Table;
import lombok.Data;

/**
 * ProductVo
 *
 * @author shengjun.hu
 * @date 2021/6/16
 */
@Data
@Table("product")
public class ProductVo {

    @Id
    private Long id;
    @Column("name_")
    private String name;
    private String barcode;
    private Long saleStatus;

}
