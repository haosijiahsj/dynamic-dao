package com.husj.dynamicdao.slavedao;

import com.husj.dynamicdao.annotations.BatchSave;
import com.husj.dynamicdao.annotations.Query;
import com.husj.dynamicdao.dao.BaseDao;
import com.husj.dynamicdao.model.ProductVo;

import java.util.List;

/**
 * ProductDao
 *
 * @author shengjun.hu
 * @date 2021/6/16
 */
public interface ProductSlaveDao extends BaseDao<ProductVo> {

    @BatchSave
    void batchSave(List<ProductVo> productVos);

    @Query
    List<ProductVo> queryALl();

}
