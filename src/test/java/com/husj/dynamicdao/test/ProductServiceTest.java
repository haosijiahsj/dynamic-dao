package com.husj.dynamicdao.test;

import com.github.jsonzou.jmockdata.JMockData;
import com.husj.dynamicdao.model.ProductVo;
import com.husj.dynamicdao.service.ProductService;
import com.husj.dynamicdao.utils.ServiceUtils;
import org.junit.Before;
import org.junit.Test;

/**
 * ProductServiceTest
 *
 * @author shengjun.hu
 * @date 2021/6/16
 */
public class ProductServiceTest {

    private ProductService productService;

    @Before
    public void setUp() {
        productService = ServiceUtils.getBean(ProductService.class);
    }

    @Test
    public void saveTest() {
        ProductVo productVo = JMockData.mock(ProductVo.class);
        productService.save(productVo);
    }

    @Test
    public void batchSaveTest() {

    }

}
