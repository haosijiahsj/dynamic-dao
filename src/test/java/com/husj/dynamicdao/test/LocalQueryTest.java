package com.husj.dynamicdao.test;

import com.github.jsonzou.jmockdata.JMockData;
import com.husj.dynamicdao.DynamicDaoTemplate;
import com.husj.dynamicdao.config.LocalDataSourceConfig;
import com.husj.dynamicdao.dao.QueryDao;
import com.husj.dynamicdao.dao.SaveDao;
import com.husj.dynamicdao.model.EntityPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * LocalQueryTest
 *
 * @author shengjun.hu
 * @date 2021/6/23
 */
@Slf4j
public class LocalQueryTest {

    private DynamicDaoTemplate dynamicDaoTemplate;

    @Before
    public void setUp() {
        dynamicDaoTemplate = new DynamicDaoTemplate(LocalDataSourceConfig.getDataSource());
    }

    @Test
    public void localTest() {
        try {
            dynamicDaoTemplate.begin();
            QueryDao queryDao = dynamicDaoTemplate.getDynamicDao(QueryDao.class);
            List<EntityPo> entityPos = queryDao.query2(1, 0);
            log.info("{}", entityPos);

            int i = 1 / 0;

            EntityPo entityPo = JMockData.mock(EntityPo.class);
            SaveDao saveDao = dynamicDaoTemplate.getDynamicDao(SaveDao.class);
            saveDao.save10(entityPo);
            dynamicDaoTemplate.commit();
        } catch (Exception e) {
            dynamicDaoTemplate.rollback();
            throw e;
        }
    }

}
