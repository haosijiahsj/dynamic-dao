package com.husj.dynamicdao.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.husj.dynamicdao.spring.DynamicDaoScan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @author 胡胜钧
 * @date 7/4 0004.
 */
@Slf4j
@Configuration
@DynamicDaoScan(value = "com.husj.dynamicdao.dao", dataSourceRef = "dataSource")
public class DynamicDaoAutowiredConfig {

    private static final String DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";

    @Bean
    public DataSource dataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(DRIVER_CLASS_NAME);
        druidDataSource.setUrl("jdbc:mysql://localhost:3306/hello-world?characterEncoding=utf-8");
        druidDataSource.setUsername("root");
        druidDataSource.setPassword("123456");

        log.info("主数据源初始化！");

        return druidDataSource;
    }

}
