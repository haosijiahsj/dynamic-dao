package com.husj.dynamicdao.annotations.mapping;

/**
 * EnumType
 *
 * @author shengjun.hu
 * @date 2021/6/9
 */
public enum EnumType {
    NONE, ORDINAL, STRING
}
