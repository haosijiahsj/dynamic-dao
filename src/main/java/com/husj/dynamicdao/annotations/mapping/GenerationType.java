package com.husj.dynamicdao.annotations.mapping;

/**
 * IdType
 *
 * @author shengjun.hu
 * @date 2021/6/8
 */
public enum GenerationType {
    IDENTITY, ASSIGNED, UUID, GENERATED
}
